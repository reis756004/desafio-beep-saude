$(document).ready(function(){
    atualiza();
  });
  
//ATUALIZA O GRÁFICO
function atualiza(){  
    Callback = "Grafico";
    Callback_Action = "carregaInicial";
    $.getJSON('ajax/Graficos.php',  {callback: Callback, callback_action: Callback_Action}, function(data) { 
        if(data){
            $( ".d-flex" ).fadeOut( 500, function() {
                $( ".d-flex" ).remove();
                carregaGrafico(data);
              });
        }
    }, 'json');
}

//AO CLICAR EM UM DOS BOTÕES DE MOEDA DA TELA, CARREGA A COTAÇÃO
$('html').on('click', '.btn', function (e) {
    Callback = "Converte";
    Callback_Action = "converteMoeda";
    Parametro = $(this).attr('paran');

    $.getJSON('ajax/Graficos.php',  {callback: Callback, callback_action: Callback_Action, parametro: Parametro}, function(data) { 
        if(data){
            $('#valor').remove();
            $("#cotacao").append("<h4 class='display-4' id='valor'>USD: 1 vs "+data.moeda + ": " + data.valor+"</h4>");
        }
    }, 'json');
});  
  
//FUNÇÃO DE CAREGAMENTO DO GRÁFICO
function carregaGrafico(data){
var dataAtual = new Date();
var wc_chart = Highcharts.chart('container', {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Cotação'
    },
    xAxis: {
        title: {
            text: 'Data'
        },
        type: 'datetime',
        labels: {
            format: '{value:%d/%m/%Y}',
            align: 'center'
        }
    },
    yAxis: {
        title: {
            text: 'Valor'
        },
        labels: {
            formatter: function () {
                return this.value;
            }
        }
    },
    tooltip: {
        pointFormat: '{series.name} valendo <b>U$ {point.y}</b><br/>no dia {point.x:%d/%m/%Y}'
    },
    plotOptions: {
        spline: {
            lineWidth: 4,
            states: {
                hover: {
                    lineWidth: 5
                }
            },
            marker: {
                enabled: false
            },
            pointStart: Date.UTC(dataAtual.getFullYear(), dataAtual.getMonth(), dataAtual.getDate() - 6),
            pointInterval: 24 * 36e5
        }
    },
    series: [
            {
            name: data.nome[0],
            data: data.valor[0]
            },{
            name: data.nome[1],
            data: data.valor[1]
            },{
            name: data.nome[2],
            data: data.valor[2]
            },{
            name: data.nome[3],
            data: data.valor[3]
            }
            ]
});
}