<?php
    session_start();
    require '../app/Config.inc.php';
    
    $jSON = null;
    $PostData = filter_input_array(INPUT_GET, FILTER_DEFAULT);

    $Callback = $PostData['callback'];
    $Callback_Action = $PostData['callback_action'];
    $Parametro = isset($PostData['parametro']) ? $PostData['parametro'] : NULL;

    $Controller = "../app/Controllers/Controller".$Callback.".class.php";
    $Classe = "Controller". $Callback;
    require_once($Controller);
    
    //Instancia o objeto do tipo
    $objController = new $Classe();
    //Chamanda de método
    if($Parametro != NULL){
        $Metodo = $objController->$Callback_Action($Parametro);  
    }else{
        $Metodo = $objController->$Callback_Action();  
    }
      
    