<?php
class ModelConverte {
    private $Read;
    
    public function  __construct(){
        $this->Read = new Read;     
    }

    //CONSULTA NO BANDO DE DADOS OS VALORES DAS MOEDAS ESCOLHIDAS
    public function moedaEscolhida($moeda){
        $dataAtual = date('Y-m-d');
        $this->Read->FullRead("SELECT * FROM cotacao WHERE moeda = :moeda AND data = :data","moeda={$moeda}&data={$dataAtual}");
        $Dados['valor'] = str_replace('.',',',$this->Read->getResult()[0]['valor']);
        $Dados['moeda'] = $this->Read->getResult()[0]['moeda'];
        return $Dados;
    } 
 }