<?php
class ModelGrafico {
    private $access_key = "7ba9f530179fe27cc83edae0bac0c83a";
    private $moedas = array('BRL', 'USD', 'EUR', 'ARS');
    private $endpoint = 'historical';    
    private $api = 'http://apilayer.net/api/';
    private $Create;
    private $Read;
    
    public function  __construct(){
        $this->Create = new Create;
        $this->Read = new Read;    
    }

    public function carregaDados(){
        
        $this->montaConsulta($this->moedas);
        $COTACAO = NULL;
        $dias = 7;
        for($i=0; $i < $dias; $i++){

            //TRATAMENTO DE DADOS ANTES DA REQUISIÇÃO   
            $date = new DateTime(" -".$i."day");          
            $dataPesquisa = $date->format("Y-m-d");
            $this->Read->FullRead("SELECT * FROM cotacao WHERE data = :data AND (" . $this->montaConsulta($this->moedas)['condicao'] . ")", "data=". $dataPesquisa. "&" . $this->montaConsulta($this->moedas)['parametro']);

            if (!$this->Read->getResult()){                
                $Response = $this->consultaApi($date);
                $Date = $Response['date'];

                //INSERÇÃO NO BANCO DE DADOS PARA ECONOMIZAR REQUISIÇÕES API 
                foreach ($Response['quotes'] as $key => $value) {
                    $COTACAO['moeda'] = substr($key, -3, 3);
                    //$COTACAO['valor'] = round(($value / $exchangeRates['quotes']['USDBRL']), 2);
                    $COTACAO['valor'] = round($value, 2);
                    $COTACAO['data'] = $Date;

                    $this->Read->FullRead("SELECT id FROM cotacao WHERE data = :data AND moeda = :moeda","data={$Date}&moeda={$COTACAO['moeda']}");            
                    if (!$this->Read->getResult())
                    { 
                        $this->Create->ExeCreate("cotacao", $COTACAO);                        
                    }               
                }
            }            
        }
        $PreparaDados = $this->preparaDados();
        return $PreparaDados;
    }

    //MONTAGEM DOS PARAMETROS PARA CONSULTA E NÃO REALIZAR REQUISIÇÃO DESNECESSÁRIA
    private function montaConsulta($array){
        $Consulta['condicao'] = NULL;
        $Consulta['parametro'] = NULL;
        for ($i=0; $i < count($array); $i++) { 
            if($i != (count($array) - 1)){
                $Consulta['condicao'] .= "moeda = :moeda" . $i . " OR ";
                $Consulta['parametro'] .= "moeda" . $i . "=". $array[$i]. "&";
            }else{
                $Consulta['condicao']  .= "moeda = :moeda" . $i;
                $Consulta['parametro'] .= "moeda" . $i . "=". $array[$i];
            }            
        }
        return $Consulta;
    }

    //REQUISIÇÃO A API CURRENCYLAYER
    private function consultaApi($date){
        $currencies = implode(",", $this->moedas);
        $ch = curl_init($this->api . $this->endpoint .'?access_key='.$this->access_key.'&date='.$date->format('Y-m-d').'&currencies='.$currencies.'&format=1');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);
        $exchangeRates = json_decode($json, true);
        
        return $exchangeRates;
    }

    //PREPARA OS DADOS PARA ENVIO A CLASSE VIEW
    private function preparaDados(){
        $moeda = array('EUR' => 'Euro', 'USD' => 'Dolar', 'BRL' => 'Real', 'ARS'=> 'Peso Argentino');
        foreach ($moeda as $key => $value) {
            $this->Read->FullRead("SELECT * FROM cotacao WHERE moeda = :moeda AND data BETWEEN DATE_SUB(now(), INTERVAL 7 DAY) AND now()", "moeda={$key}");
            if ($this->Read->getResult()){  
                for ($i=0; $i < count($this->Read->getResult()); $i++) { 
                    $v[$i] = $this->Read->getResult()[$i]['valor'];
                    $d[$i] = $this->Read->getResult()[$i]['data'];
                }         
                $json['nome'][] = $value;
                $json['valor'][] = [(float)$v[0], (float)$v[1], (float)$v[2], (float)$v[3], (float)$v[4], (float)$v[5], (float)$v[6]];                
            }
        } 
        return $json;
    } 
 }