<?php
class ConvertTest extends PHPUnit_Framework_TestCase{

    public function testMoedaEscolhida(){
        $moeda = "USD";
        $teste = new ModelConverte();
        $teste->moedaEscolhida($moeda);

        $esperadoValor = "1";
        $esperadoMoeda = "USD";
        $this->assertEquals($esperadoValor, $teste->moedaEscolhida($moeda)['valor']);
        $this->assertEquals($esperadoMoeda, $teste->moedaEscolhida($moeda)['moeda']);


    }

}