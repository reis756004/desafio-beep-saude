<?php
class ControllerConverte {
    private $View;
    private $Model;

    public function  __construct(){
        $this->Model = new ModelConverte;
        $this->View = new ViewConverte;            
    }

    public function converteMoeda($moeda){
        $this->Model->moedaEscolhida($moeda);
        $this->View->mostrarCotacao($this->Model->moedaEscolhida($moeda)); 
    }
} 