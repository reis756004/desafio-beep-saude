<?php
class ControllerGrafico {
    private $View;
    private $Model;

    public function  __construct(){
        $this->Model = new ModelGrafico;
        $this->View = new ViewGrafico;            
    }

    public function carregaInicial(){
        $this->Model->carregaDados();
        $this->View->mostrarDados($this->Model->carregaDados()); 
    }
} 