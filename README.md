# DESAFIO BEEP SAUDE

> Execução do desafio proposta pela Beep Saúde

### Instalação
- Clone o projeto;
- Importe no Mysql o arquivo [beep.sql];
- Configure o arquivo [Config.sample.php](https://bitbucket.org/reis756004/desafio-beep-saude/src/master/app/Config.sample.php) na pasta app/ com os dados de conexão com o banco de dados.
- Pronto!
  
### Tecnologias

* PHP OO
* MySQL
* HTML5
* CSS
* Bootstrap
* Ajax
* jQuery
* Highcharts
* PDO
* MVC