-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Jan-2019 às 00:47
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beep`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cotacao`
--

CREATE TABLE `cotacao` (
  `id` int(11) NOT NULL,
  `moeda` varchar(5) NOT NULL,
  `valor` float NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cotacao`
--

INSERT INTO `cotacao` (`id`, `moeda`, `valor`, `data`) VALUES
(1, 'BRL', 3.76, '2019-01-29'),
(2, 'USD', 1, '2019-01-29'),
(3, 'EUR', 0.88, '2019-01-29'),
(4, 'ARS', 37.12, '2019-01-29'),
(5, 'BRL', 3.76, '2019-01-28'),
(6, 'USD', 1, '2019-01-28'),
(7, 'EUR', 0.88, '2019-01-28'),
(8, 'ARS', 37.12, '2019-01-28'),
(9, 'BRL', 3.77, '2019-01-27'),
(10, 'USD', 1, '2019-01-27'),
(11, 'EUR', 0.88, '2019-01-27'),
(12, 'ARS', 37.16, '2019-01-27'),
(13, 'BRL', 3.78, '2019-01-26'),
(14, 'USD', 1, '2019-01-26'),
(15, 'EUR', 0.88, '2019-01-26'),
(16, 'ARS', 37.2, '2019-01-26'),
(17, 'BRL', 3.77, '2019-01-25'),
(18, 'USD', 1, '2019-01-25'),
(19, 'EUR', 0.88, '2019-01-25'),
(20, 'ARS', 37.09, '2019-01-25'),
(21, 'BRL', 3.77, '2019-01-24'),
(22, 'USD', 1, '2019-01-24'),
(23, 'EUR', 0.88, '2019-01-24'),
(24, 'ARS', 37.38, '2019-01-24'),
(25, 'BRL', 3.77, '2019-01-23'),
(26, 'USD', 1, '2019-01-23'),
(27, 'EUR', 0.88, '2019-01-23'),
(28, 'ARS', 37.52, '2019-01-23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ws_config`
--

CREATE TABLE `ws_config` (
  `conf_id` int(11) UNSIGNED NOT NULL,
  `conf_key` varchar(255) DEFAULT '',
  `conf_value` varchar(255) DEFAULT '',
  `conf_type` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cotacao`
--
ALTER TABLE `cotacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_config`
--
ALTER TABLE `ws_config`
  ADD PRIMARY KEY (`conf_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cotacao`
--
ALTER TABLE `cotacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ws_config`
--
ALTER TABLE `ws_config`
  MODIFY `conf_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
